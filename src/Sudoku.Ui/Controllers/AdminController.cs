using Microsoft.AspNetCore.Mvc;

namespace Sudoku.Ui.Controllers;

public class AdminController : Controller
{
    private readonly ILogger<HomeController> _logger;
    public AdminController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }
    public IActionResult Index()
    {
        return View();
    }
}
