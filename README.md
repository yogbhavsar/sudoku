# sudoku

A simple web app with 2 pages: admin and play. Admin page will take the sudoku puzzle and save it. Play page will show that and give the user a chance to solve that. Both of these pages will have save buttons clicking on which only will the user be able to save the puzzle.
A sudoku puzzle can be taken by any number of players, the app will save the puzzle for all of them. It will take the user id from a textbox, to keep it simple.

## Out of scope
Authentication and authorization.

## Tech stack
Web App: .NET MVC 6
Database: MongoDB
Deployment: Docker
